﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Agent : MonoBehaviour {
    public float moveSpeed = 1;
    public float rotateSpeed = 1;

    public char team = 'A';
    
    public List<Behaviour> behaviours;

    private Manager _manager;
    private List<Agent> _agentCache;

    private Vector3 _target;
    private List<Vector4> _subTargets;

    void Start() {
        _manager = GetComponentInParent<Manager>();
        _agentCache = _manager.allAgents.Where(agent => agent != this).ToList();

        _subTargets = new List<Vector4>();

        var color = Color.white;
        switch (team) {
            case 'A':
                color = new Color(.88f, .44f, .15f);
                break;
            case 'B':
                color = new Color(.2f, .23f, .88f);
                break;
        }

        GetComponent<SpriteRenderer>().color = color;
    }

    // Update is called once per frame
    void Update() {
        var delta = Time.deltaTime;
        var tf = transform;
        var position = tf.position;

        _target = Vector3.zero;
        _subTargets.Clear();

        var inRange = _agentCache.Where(agent => (agent.transform.position - position).magnitude < 3).ToList();

        foreach (var behaviour in behaviours) {
            var dir = behaviour.Compute(this, inRange);

            _subTargets.Add(dir.Weighted(behaviour.weight));
        }

        _target = _subTargets.SlerpByWeight();

        var targetLook = Quaternion.LookRotation(Vector3.forward, _target);
        var rot = Quaternion.Slerp(tf.rotation, targetLook, delta * rotateSpeed);

        tf.rotation = rot;

        position += tf.up * (moveSpeed * delta);
        tf.position = position;
    }

    private void OnDrawGizmosSelected() {
        if (!Application.isPlaying) return;
        var pos = transform.position;

        Gizmos.color = Color.red;
        Gizmos.DrawLine(pos, pos + _target);

        Gizmos.color = Color.cyan;
        foreach (var target in _subTargets) {
            Gizmos.DrawLine(pos, pos + target.XYZ());
        }
    }
}
