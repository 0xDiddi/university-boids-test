﻿using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

    public GameObject prefab;
    public int amount;

    public List<Agent> allAgents;

    void Start() {
        allAgents = new List<Agent>(amount);

        var half = amount / 2;
        for (var i = 0; i < amount; i++) {
            var rot = Quaternion.Euler(0, 0, Random.value * Mathf.PI);

            var a = Instantiate(prefab, Random.insideUnitCircle, rot, transform);
            var agent = a.GetComponent<Agent>();
            // agent.team = i < half ? 'A' : 'B';
            allAgents.Add(agent);
        }


        // allAgents = GetComponentsInChildren<Agent>().ToList();
    }
}
