﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class NoisyBehaviour : Behaviour {

    [Range(0, 1)]
    public float factor = .1f;
    public override Vector3 Compute(Agent current, IEnumerable<Agent> otherAgents) {
        var dir = Random.insideUnitCircle.normalized;

        return Vector3.Slerp(current.transform.up, dir, factor);
    }
}