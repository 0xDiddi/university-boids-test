﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Behaviour : ScriptableObject {
    public float weight;
    public abstract Vector3 Compute(Agent current, IEnumerable<Agent> otherAgents);
}