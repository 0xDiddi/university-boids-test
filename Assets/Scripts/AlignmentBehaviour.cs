﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class AlignmentBehaviour : Behaviour {
    public override Vector3 Compute(Agent current, IEnumerable<Agent> otherAgents) {
        var others = otherAgents.ToList();
        return others.Count == 0
            ? Vector3.zero
            : others.Select(agent => agent.transform.up.Weighted(1)).ToList().SlerpByWeight();
    }
}
