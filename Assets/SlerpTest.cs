﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SlerpTest : MonoBehaviour {
    
    public List<Vector2> angles;

    private List<Vector4> _vectors;
    
    public Vector3 _avgByVector;

    private void OnValidate() {
        _vectors = angles.Select(angle => new Vector3(Mathf.Sin(angle.x), Mathf.Cos(angle.x)).Weighted(angle.y)).ToList();

        _avgByVector = _vectors.SlerpByWeight();
    }

    private void OnDrawGizmosSelected() {
        foreach (var vec in _vectors) {
            Gizmos.DrawLine(Vector3.zero, vec);
        }

        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(Vector3.zero, _avgByVector);
    }
}
